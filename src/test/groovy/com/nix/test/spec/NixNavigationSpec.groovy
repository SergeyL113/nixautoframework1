package com.nix.test.spec

import geb.spock.GebReportingSpec
import com.nix.test.page.*

class NixNavigationSpec  extends GebReportingSpec {

    def "User navigates to the Blog Page"() {

        when:
            to StartPage

        and:
            "User navigates to Blog page"()

        then:
            at BlogPage
    }

}

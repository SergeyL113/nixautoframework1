package com.nix.test.page

import geb.Page

class BlogPage extends Page{

    static at = {
        title == "Web, Apps & Software Development Blog"
    }
}

package com.nix.test.page

import geb.Page

class StartPage  extends Page{

    static at = {
        title.contains("Outsourcing Offshore Software Development Company")
    }

    static content = {
            BlogLink(wait:true) { $("li#menu-item-6321 a") }
    }

    def "User navigates to Blog page"(){
        BlogLink.click()
    }

}

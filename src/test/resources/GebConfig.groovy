import org.openqa.selenium.UnexpectedAlertBehaviour
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities

System.setProperty("geb.build.reportsDir", "target/geb-reports")
System.setProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver.exe")
System.setProperty("geb.build.baseUrl", "https://www.nixsolutions.com/")

driver = {
    ChromeOptions options = new ChromeOptions()
    options.addArguments("--start-maximized")
    options.addArguments("window-size=1440x900")
    DesiredCapabilities capabilities = DesiredCapabilities.chrome()
    capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT)
    capabilities.setCapability(ChromeOptions.CAPABILITY, options)
    def driver = new ChromeDriver(capabilities)
    return driver
}